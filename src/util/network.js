const callback = process.env.SERVER || `${window.location.origin}${window.location.pathname || ''}`
const relayUrl = process.env.RELAY_URL

const relURL = url=>{
	if(callback.length){
		const relative = new URL(document.baseURI).origin === new URL(url, document.baseURI).origin
		if(relative) url = (new URL(url, callback)).toString()
	}
	return url
}

/*
Enhanced fetch
- relativizes url on process.env.SERVER
- forwards data the right way automatically, based on Content-Type header
*/
const fetch2 = (url, opts={})=>{
	let callingURL = relURL(url)
	if(url !== callingURL) opts.mode = 'cors'

	if(!opts.method || opts.method === 'GET'){
		if(opts.data) callingURL += '?' + (new URLSearchParams(opts.data)).toString()
	}else if(opts.headers){
		switch(opts.headers['Content-Type']){
			case 'application/x-www-form-urlencoded':
				if(opts.data) opts.body = (new URLSearchParams(opts.data)).toString()
				break;
			case 'application/json':
				if(opts.data) opts.body = JSON.stringify(opts.data)
				break;
		}
	}
	return fetch(callingURL, opts)
}
const sse = async (url, events={})=>{
	return new Promise((a, r)=>{
		const sse = new EventSource(relURL(url))
		sse.addEventListener('error', ({data}={})=> r(data))
		sse.addEventListener('close', ({data}={})=>{
			sse.close()
			a(data)
		})
		for(const [name, fn] of Object.entries(events)){
			sse.addEventListener(name, ({data}={})=> fn(data))
		}
	})
}
const relay = data=> fetch2(`${relayUrl}/relay`, {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	data: {...data, origin: window.location.origin}
})
const forward = (url, data={})=> fetch2(`${relayUrl}/forward`, {data: {url, ...data}})

module.exports = {
	relURL,
	fetch: fetch2,
	sse,
	relay, forward
}
